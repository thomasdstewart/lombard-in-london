---
title: "About"
description: "I am Alessandra, an Italian girl in London."
featured_image: "/images/FB_20150713_18_54_55_Saved_Picture_crop.jpg"
---
Hi!

I am Alessandra, an Italian girl in London. I moved from the Lombardy region in Italy to finish my studies in London in 2014. 
Straightaway, I have fallen in love with London and everything this city has to offer: museum, parks, events, exhibitions, etc. 
I was also very lucky that I also met here (my now husband) Thomas! 

I'am so in love with this wonderful city without forgetting my Italian origins. 
I work as scientist on vaccines, but in my spare time I love visiting London, travelling and cooking with my husband. 

I have created this blog to share with you some of the places we love (in London but also abroad), tips, day trips, recipes we like, anything we enjoy in life!

Hope you enjoy reading this blog, I definetly enjoy writing it!


{{< figure src="/images/FB_20150713_18_54_55_Saved_Picture.jpg">}}
