---
title: "County Clare"
date: 2018-03-30
tags: ["Travel","Ireland"]
featured_image: "/images/20170705_152736-e1522437696864.jpg"
---

Thomas' parents have a house in Ireland, in a small town on the West Coast. 
If you love nature as we do, Ireland is truly the right place for you! It is such a lovely and welcoming country filled with traditional music and lovely people.

It's also the best place where to detach from reality. Since internet coverage is ok, but limited, and broadband is not very fast, you can finally read that book you had on your bedside table for so long and enjoy the birds, the sea, the views leaving your phone at home! 

However, there are also lots of things to see around! When we come to Ireland we always rent a car, as it's pretty much the only and the best way to move around. You can cycle if you like, but the weather is a bit of a question mark, so... we don't want to risk it! In addition, being italian, I am not a fan of the cold and rainy weather, but I suppose this is what contributes those magnificent landscapes!

The house of Thomas' parents is in a small town called Milltown Malbay, which is know for a traditional Irish music festival that takes place at the end of June called "Willie Clansy", where thousand of people come to the area and traditional Irish music is played all day (and night) round. Their house is not far from the ocean and believe me, it is stunning! Famous beaches nearby are Lahinch, the Spanish point and Whitestrand, which are all beautiful and hold a space plate in Thomas' heart, as he used to spend his summer holiday there when he was a child.

You could be very lucky with the weather and have really great days in Ireland, but you need to be ready for the famous "horizontal drizzle"...which means that because of the wind... it rains horizontally! Make sure you have waterproof clothes and jacket, with many layers to keep you warm.

I do really love to come here to relax and do walks on the coast and to the beach. However, you can't miss these places if you are planning a trip there: 
- The Cliffs of Moher
- Doolin' Caves 
- Ailwee Caves 
- Driving along the old coastal route called the "wild Atlantic way", really deserves a go... 

I have attached few pictures! Enjoy Ireland!

{{< figure src="/images/20180329_153032-e1522437384585.jpg">}}
{{< figure src="/images/20180329_152709-e1522439000755.jpg">}} --Lahinch
{{< figure src="/images/20170705_152736-e1522437696864.jpg">}} --Cliffs of Moher
{{< figure src="/images/20180329_160743.jpg">}} --Whitestrand
{{< figure src="/images/20180329_200524-e1522438813806.jpg">}} --View from Thomas' parents house
{{< figure src="/images/20170707_155501-e1522439452776.jpg">}} --Tea and Garden rooms in Ballyvaughan... a must visit for a sweet break!

