---
title: "Choose the right date... for you"
date: 2019-08-30
tags: ["Wedding","Date"]
featured_image: "/images/Screenshot_20190830-061005_Samsung_Experience_Home_crop.jpg"
---

When it comes to organising a wedding, the first thing to think about is when!
Do you like spring, summer, autumn or winter months? Have you always dreamt of a wedding in the summer, on a seaside location?
Or with the snow? Are your favourite flowers blossoming in spring or late summer?

For us, we were initially thinking to pick a bank holiday, so our anniversary would have always fallen always on a holiday! I definetly wanted to be in the summer, so August sounded like the right month. Another positive tick because I love sunflowers and August is the perfect time for them! However, since we got engaged in August 2017 and we were organising our wedding abroad, in Italy, it was hard to think about getting married the year after in August, as in Italy locations get booked out around 15-18 months before, so the choice would have been quite limited for 2018!

If I can make a suggestion, go for something meaningful to you as a couple. Something you'll remind forever.
For us, we decided to get married on the 30th August 2019, exactly five years after our first date (and kiss)!
It was a Friday, which suited well our guests, so people had a long weekend or took the bank holiday week off to travel to Italy.
Also, the day was filled with lots of memories from our first date... it was awesome to look back and see how far we got to in 5 years!

And remember, enjoy it all! From the preparation, to the actual wedding day, the honeymoon... every little thing count and you're only going to live this once (hopefully!)

Lots of luck... and love!

{{< figure src="/images/Screenshot_20190830-061005_Samsung_Experience_Home.jpg">}}
