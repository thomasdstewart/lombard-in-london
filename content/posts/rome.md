---
title: "Rome"
date: 2018-09-16
tags: ["Travel","Italy"]
featured_image: "/images/IMG_20180508_221242_834.jpg"
---
Rome, in Italy, it is also known as the Eternal City. And believe me, it truly is. There is so much history at every corner and if you love architecture, Rome is the place to visit! There are hundreds of churches at every corner and even if you are not religious, you'll agree that their paintings, frescoes and architectures are stunning and deserve a visit!

We loved every corner of this city! We went as a family trip (us two plus my parents and Thomas ' parents) and stayed in an Airbnb close to the Spanish Steps... we love Airbnb and this one in particular was stunning and veru centrale! We pretty much walked everywhere, which was probably the best way to go around Rome. For us it worked wonderfully! Also, the airbnb gives you so much freedom! One evening for example we were so tired that we got some lovely Italian food in a shop and order pizzas and staying in the flat, which had a lovely dining room.

There is so much to do and to see in Rome. We bought some Roma passes to enter the major monuments and attractions. It was worth the money for us since we entered to all the places we could possibly access, however it might no be depending on your choices of visit! We were able to enter the Colosseum, for example, and skip the queue. Some of the things I enjoyed more have been the Colosseum, going up the Vittoriano (worth paying the extra 10 euro for the lift till the top!), climbing St Peter's dome (beware of the many steps), listening to the Angelus by the Pope on Sunday, visiting Castel Sant'Angelo, the Sistine chapel and the Vatican's museums! And honestly, this is just half of what we saw! I think 4-5 nights is the minimum required for visiting Rome since it's such a massive city! We use DK eyewitnesses travel books and honestly, they are my favourites, since they are so full of tips and advice. For dinner, we looked at places on TripAdvisor and we have found lovely gems (my worry was to ended up paying lots for a meal! Instead, TripAdvisor was really helpful) and for lunch we had sandwiches or focaccia (Italian bread) filled with affettati. We ended up buying the Omnia Car which worked well for us, but only because we did everything that was on the list available.  For the majority of museums you need to plan ahead as especially the Vatican's museums have huge amount of visitors per day. Honestly, I would have like to spend more time in them but it took us 2 hours to get to  the Sistine chapel because of the queue! However, so so worth it! If you have a few days, Rome is the place to put on your list... a must do!


{{< figure src="/images/IMG_20180506_232920_780.jpg">}}
{{< figure src="/images/IMG_20180508_221606_437.jpg">}}
{{< figure src="/images/IMG_20180508_221242_834.jpg">}}
{{< figure src="/images/IMG-20180507-WA0045.jpg">}}
{{< figure src="/images/20180507_233522-e1537137527687.jpg">}}
{{< figure src="/images/20180506_145312-e1537137543754.jpg">}}
{{< figure src="/images/20180507_104625-e1537137435556.jpg">}}
