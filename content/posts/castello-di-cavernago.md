---
title: "Our wedding venue: il Castello di Cavernago (BG, Italy)"
date: 2019-08-30
tags: ["Wedding","Venue"]
featured_image: "/images/castello_di_cavernago_2018.jpg"
---
One of the first things we did after choosing the date of our marriage, was to start looking for the perfect location where to celebrate our wedding day! 
I am from the North of Italy, from a little town in the county of Bergamo (BG), so we wanted to celebrate our marriage in Italy. However, in order to make the journey accessible to all our guests, some of which were coming from the UK, Ireland, Netherlands, etc., we were looking for a venue which had a church close by where to celebrate a catholic wedding and also that wasn't too far from the airport/hotel.

We visited five venues between January and March 2018, all of which were stunning. However, you really know when you visit the right venue for you! We felt in love with the Castle of Cavernago (Castello di Cavernago; https://www.residenzedepoca.it/matrimoni/s/location/castello_di_cavernago/) and the little catholic church right at the front of the Castle as soon as we got there. The Castle was built in the 14th century as the summer residence for the Lord of Bergamo, Bartolomeo Colleoni, and his family. The place itself is stunning... it has frescoes on the walls and ceilings which make it so romantic! 
The Castle now belongs to the Gonzaga family and it's only used for weddings, so unfortunately it's not normally open to the public. However, it can be seen from the outsite by driving through Cavernago town. This small town is just south of Bergamo and very close to the Milan-Bergamo Orio al Serio airport which worked best for our guests. 

Longhi Banqueting (https://www.longhibanqueting.it/) is the company responsible for organizing weddings in this venue and we can only recommend them. We live just outside London and I have never wanted to hire someone else to organise my own wedding. With them, everything was straight forward and mostly done online. I visited them only a few times in person when I went to see my parents but all the last checks were done two days before the wedding, when we dropped all our decorations, cake topper, etc. 

If I could live this day all over again, I'll pick this place and team Longhi again!

{{< figure src="/images/castello_di_cavernago_2018.jpg">}}
{{< figure src="/images/20180317_114305_007-e1522415578175.jpg">}}

