---
title: "Our wedding day"
date: 2019-08-30
tags: ["Wedding","Wedding day"]
featured_image: "/images/Thomas_e_Alessandra_10413.jpg"
---

Our wedding day was the best day in my life so far.

I had a tiny mental breakdown the night before the wedding, when I was very stressed hoping everything was going to be amazing. Also, I was terribly missing Thomas, who slept in the hotel while I stayed at my parents' house. I think every bride experienced that at some point during the wedding preparation, for me that happened the night before my wedding. However, after a good sleep, I was magically recharged. 

I woke up at 6.30am and I opened Thomas' card. It contained a link to lots of videos he took every day of the week before our wedding and they were so moving. I was so looking forward to see him at the altar.

I then had a nice breakfast and a little chat with my mum. I took a shower and then I got my brother to drive me to the hairdresser (which unfortunately was not able to come to the house). She was spot on and did my hair wonderfully. The hairstyle lasted all day! 
We then got back to my parents' house, where the make up artist was waiting for me. I had a little time to have a sneaky look at the florist van, full of flowers, before the florist finished to arrange the car and went to decorate the church and venue.
The make up artist was amazing and again, the make up lasted all day despite the hot weather. She also left me a little kit in case I had to adjust the make up, however, there was no need!
By 9.30am, the photographers arrived. I put the dress on, which was amazing, I was so happy. We took lots of photos and it was very fun. Remember, it is your day!
By 10.30, we left the house to go to the church. I didn't want any big or old car, so we used my brother's car. We put some songs using Spotify and laugh and sing all the way. I have truly the best memories of that journey.  
My dad walked my down the aisle and seeing Thomas was so emotional. I was so happy that everyone we love was there, celebrating with us.
The ceremony was at 11.30, in both English and Italian languages. I will never forget those feelings and the workds of Fr. Norbert, our priest.

After mass, we all walked to the venue, the Castle of Cavernago. The buffet was amazing (we were too busy taking photos!), but we sat down to eat the food which was incredible. We took more photos, we laugh a lot. The speaches were unforgettable. I did one, then Thomas, then his brother and my dad.

I smiled all day long and I was so so happy. Remember, it is your day and everyone is there to celebrate with you.
Enjoy the whole day. You planned this day for ages but it will be even better than your plans. Just embrace it.
It will fly so quickly, so my suggestion is to take a moment with your husband, just to sit back and to watch your guests. Keep these memories in your brain. It's the most amazing day in your lives.

{{< figure src="/images/Thomas_e_Alessandra_10413.jpg">}}
{{< figure src="/images/Thomas_e_Alessandra_10697.jpg">}}
{{< figure src="/images/Thomas_e_Alessandra_10520.jpg">}}
{{< figure src="/images/Thomas_e_Alessandra_10644ok.jpg">}}
{{< figure src="/images/Thomas_e_Alessandra_10669.jpg">}}

