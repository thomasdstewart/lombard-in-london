---
title: "Welcome to my blog"
date: 2018-02-22
featured_image: "/images/20170827_192804.jpg"
---

Hi,

This is my first post on my first ever blog, so it's difficult to know where to start!

I am Alessandra and I am Italian. I moved to London almost 5 years ago for a 6 months Master thesis' project and I just felt so much in love with London (and found my husband to be!) that I have decided to stay a bit longer! I have never imagined my life would have been so surprising!

I am originally from a very small village near Bergamo, in the region of Lombardy (not too far from Milan)... this is where the name of my blog comes from! So you can imagine the shock of moving to a huge metropoly as London! But I think that life is always full of opportunities (and adventures) and sometimes you just need to grab them, jumping out of your comfort zone, perhaps not neither knowing exactly where they will bring you...! Because usually is somewhere exciting, new and adventurous, giving you a better and a new understanding of yourself! Plus, you get to know, to see, to try lots of new things, you'll make new friends and... if you are as lucky as me, you may also find a husband ;)

This is us on our engagement day (27 August 2017).. Thomas proposed on the Golden Jubilee Bridge, a bridge in London which overlook the London eye on the left and the Big Ben on the right, just between Embankment and Waterloo... we love this bridge and he couldn't choose any better place to ask me to marry him!


{{< figure src="/images/20170827_192804.jpg">}}
