---
title: "Sky Garden (London)"
date: 2018-03-30
---
As this page is called Lombard in London, you were probably wondering why I haven't talked about London yet... it took me a while to actually sit down and think to which is my favourite place is London (as there are quite a few!)... honestly, there is so much to choose from that it has been very hard!

However, I think that the Sky Garden (https://skygarden.london) could be in my top 10. This garden is at the top floor of the "Walkie-Talkie" skyscraper (20 Fenchurch Street) in the City of London financial district. From here, you could see almost all London, through its 360°C views over the city!

As from the name, it is a garden... in the sky! The garden is full of different plants and vary over the year but it's always very pretty and well kept! We bring here all the people who come to visit us in London as the view is spectacular! In addition, it's free! However, tickets sold out very quickly and booking only open two weeks in advance... a shame!

I have attached few pictures!

Ale

{{< figure src="/images/20180210_173821.jpg">}}
{{< figure src="/images/20180210_174135-e1522440665250.jpg">}}
{{< figure src="/images/20170417_184126.jpg">}}
{{< figure src="/images/IMG_20150726_181057655_HDR.jpg">}}
